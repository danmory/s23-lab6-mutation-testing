from calculator import count
import random


def test_valid_sum():
    a = random.randint(0, 100000)
    b = random.randint(0, 100000)
    assert count(a, b, '+') == (a+b)


def test_valid_sum_float():
    a = 2.5
    b = 11.32
    assert count(a, b, '+') == (a+b)


def test_valid_sum_negative_test():
    a = 2.5
    b = -11.32
    assert count(a, b, '+') == (a+b)


def test_valid_sub():
    a = random.randint(0, 100000)
    b = random.randint(0, 100000)
    assert count(a, b, '-') == (a-b)


def test_valid_sub_float():
    a = 7.3
    b = 2.1
    assert count(a, b, '-') == (a-b)


def test_valid_sub_negative_float():
    a = 7.3
    b = 11.1
    assert count(a, b, '-') == (a-b)


def test_valid_div():
    a = random.randint(0, 100000)
    b = random.randint(0, 100000)
    assert count(a, b, '/') == (a/b)


def test_valid_div_float():
    a = 22
    b = 33
    assert count(a, b, '/') == (a/b)
    assert count(b, a, '/') == (b/a)


def test_valid_div_negative_float():
    a = -22
    b = 33
    assert count(a, b, '/') == (a/b)
    assert count(b, a, '/') == (b/a)


def test_valid_mult():
    a = random.randint(0, 100000)
    b = random.randint(0, 100000)
    assert count(a, b, '*') == (a*b)


def test_valid_mult_float():
    a = 0.5
    b = 0.7
    assert count(a, b, '*') == (a * b)


def test_valid_mult_negative_float():
    a = -0.5
    b = 0.7
    assert count(a, b, '*') == (a * b)
    assert count(b, a, '*') == (b * a)


def test_div_by_zero():
    a = random.randint(0, 100000)
    b = 0
    assert count(a, b, '/') == "Division by zero!"
    assert count(b, a, '/') == 0


def test_invalid_sign():
    a = random.randint(0, 100000)
    b = random.randint(0, 100000)
    checked = 0
    try:
        count(a, b, "{")
    except:
        assert True
        checked += 1
    try:
        count(a, b, "}")
    except:
        assert True
        checked += 1
    try:
        count(a, b, "[")
    except:
        assert True
        checked += 1
    try:
        count(a, b, "]")
    except:
        assert True
        checked += 1
    try:
        count(a, b, "(")
    except:
        assert True
        checked += 1
    try:
        count(a, b, ")")
    except:
        assert True
        checked += 1
    assert checked == 6
