from bonus_system import calculateBonuses


def test_standard_program():
    assert calculateBonuses("Standard", 5000) == 0.5
    assert calculateBonuses("Standard", 10000) == 0.5 * 1.5
    assert calculateBonuses("Standard", 20000) == 0.5 * 1.5
    assert calculateBonuses("Standard", 50000) == 0.5 * 2
    assert calculateBonuses("Standard", 70000) == 0.5 * 2
    assert calculateBonuses("Standard", 100000) == 0.5 * 2.5
    assert calculateBonuses("Standard", 200000) == 0.5 * 2.5

def test_premium_program():
    assert calculateBonuses("Premium", 5000) == 0.1
    assert calculateBonuses("Premium", 10000) == 0.1 * 1.5
    assert calculateBonuses("Premium", 20000) == 0.1 * 1.5
    assert calculateBonuses("Premium", 50000) == 0.1 * 2
    assert calculateBonuses("Premium", 70000) == 0.1 * 2
    assert calculateBonuses("Premium", 100000) == 0.1 * 2.5
    assert calculateBonuses("Premium", 200000) == 0.1 * 2.5

def test_diamond_program():
    assert calculateBonuses("Diamond", 5000) == 0.2
    assert calculateBonuses("Diamond", 10000) == 0.2 * 1.5
    assert calculateBonuses("Diamond", 20000) == 0.2 * 1.5
    assert calculateBonuses("Diamond", 50000) == 0.2 * 2
    assert calculateBonuses("Diamond", 70000) == 0.2 * 2
    assert calculateBonuses("Diamond", 100000) == 0.2 * 2.5
    assert calculateBonuses("Diamond", 200000) == 0.2 * 2.5

def test_invalid_program():
    assert calculateBonuses("", 5000) == 0
    assert calculateBonuses("", 10000) == 0
    assert calculateBonuses("", 20000) == 0
    assert calculateBonuses("", 50000) == 0
    assert calculateBonuses("", 70000) == 0
    assert calculateBonuses("", 100000) == 0
    assert calculateBonuses("", 200000) == 0
    assert calculateBonuses("Abc", 5000) == 0
    assert calculateBonuses("Zbc", 5000) == 0



